package com.atlassian.jira.toolkit.customfield.searchers;

import com.atlassian.jira.issue.customfields.searchers.ExactTextSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;

/**
 * @since JIRA 4.0
 */
public class DomainSearcher extends ExactTextSearcher implements CustomFieldStattable
{
    public DomainSearcher(JqlOperandResolver jqlOperandResolver, final CustomFieldInputHelper customFieldInputHelper)
    {
        super(jqlOperandResolver, customFieldInputHelper);
    }

    public StatisticsMapper getStatisticsMapper(CustomField customField)
    {
        return new TextStatisticsMapper(customField);
    }
}
