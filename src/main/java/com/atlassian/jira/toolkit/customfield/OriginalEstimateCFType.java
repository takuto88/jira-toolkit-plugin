package com.atlassian.jira.toolkit.customfield;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.velocity.NumberTool;

import java.util.Map;

public class OriginalEstimateCFType extends CalculatedCFType implements SortableCustomField
{
    private static final Double NO_ESTIMATE = new Double(0);

    public OriginalEstimateCFType()
    {
    }

    public String getStringFromSingularObject(Object value)
    {
        return value != null ? value.toString() : "0";
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        if (string != null)
        {
            return new Double(string);
        }
        else
        {
            return NO_ESTIMATE;
        }
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        if (issue != null)
        {
            Long originalEstimate = issue.getOriginalEstimate();
            if (originalEstimate != null)
            {
                return new Double(originalEstimate.doubleValue() / (double) 3600);
            }
        }

        return NO_ESTIMATE;
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem)
    {
        Map<String, Object> map = super.getVelocityParameters(issue, null, null);
        map.put("numberTool", new NumberTool());

        map.put("estimate", getOriginalEstimateString(issue));
        return map;
    }

    private String getOriginalEstimateString(Issue issue)
    {
        long estimate = -1;

        if (issue != null)
        {
            if (issue.getEstimate() != null && issue.getOriginalEstimate() != null)
            {
                estimate = Math.min(issue.getOriginalEstimate().longValue(), issue.getEstimate().longValue());
            }

            if (estimate != -1)
            {
                return DateUtils.getDurationString(estimate, getHoursPerDay(), getDaysPerWeek());
            }
        }

        return null;

    }

    private int getDaysPerWeek()
    {
        return Integer.parseInt(ComponentAccessor.getApplicationProperties().getDefaultBackedString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK));
    }

    private int getHoursPerDay()
    {
        return Integer.parseInt(ComponentAccessor.getApplicationProperties().getDefaultBackedString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY));
    }
}
